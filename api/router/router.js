const express = require('express');
const gameCtrl = require('../controller/games.controller');

const router = express.Router();

router.route("/games")
    .get(gameCtrl.gamesGetAll);
router.route("/games/:gameId")
    .get(gameCtrl.gamesGetOne);

module.exports = router;