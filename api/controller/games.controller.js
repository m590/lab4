const mongoose = require('mongoose');
const Game = mongoose.model("Game");

module.exports.gamesGetAll = function (req, res) {
    let count = 1;
    if (req.query && req.query.count) {
        count = parseInt(req.query.count);
    }
    let offset = 0;
    if (req.query && req.query.offset) {
        offset = parseInt(req.query.offset);
    }

    Game.find().skip(offset).limit(count).exec(function (err, games) {
        res.status(200).json(games);
    });

}

module.exports.gamesGetOne = function (req, res) {
    const gameId = req.params.gameId;
    Game.findById(gameId).exec(function (err, game) {
        res.status(200).json(game);
    });
}
